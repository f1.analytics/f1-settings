module.exports = {
  collectCoverage: true,
  roots: [
    "<rootDir>/src"
  ],
  testMatch: [
    "**/?(*.)+(spec|test).ts"
  ],
  setupFilesAfterEnv: [
    "./src/__tests__/setupTests.ts"
  ],
  modulePathIgnorePatterns: [
    "./src/__tests__"
  ],
  transform: {
    "^.+\\.ts": "ts-jest"
  },
};
