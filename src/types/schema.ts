export interface ISaveDataQueryOptionInput {
    name: string;
    script: string;
    variables: IDataQueryOptionVariable[];
}

export interface IPutDataQueryOptionInput extends ISaveDataQueryOptionInput {
    id: number;
}

export interface IDataQueryOptionVariable {
    name: string;
    type: DataQueryVariableTypes;
    availableValues?: Array<string | number> | null;
    exampleValue?: string | number;
}

export enum DataQueryVariableTypes {
    String = 'string',
    Number = 'number',
    NumberList = 'number-list',
}

export interface ISaveDashboardTypeInput {
    icon: string;
    name: string;
}

export interface IPutDashboardTypeInput extends ISaveDashboardTypeInput {
    id: number;
}

export interface ISaveSubjectSelectionInput {
    dashboardType: IPutDashboardTypeInput;
    config: ISubjectSelectionConfig;
    name: string;
}

export interface IPutSubjectSelectionInput extends ISaveSubjectSelectionInput {
    id: number;
}

export interface ISubjectSelectionConfig {
    selectionTypePlaceholder: string;
    filterSelect?: ISelectConfig;
    subjectSelect: ISelectConfig;
}

export interface ISelectConfig {
    placeholder: string;
    dataQuery: IDataQuery;
    variables: IVariable[];
}

export interface IDataQuery {
    id: number;
    variables: IVariable[];
}

export interface IVariable {
    exportAs: string;
    exportFrom?: string;
    exportKey?: string;
    exportValue?: number | string;
}

export interface ISaveDataCardTypeInput {
    dashboardType: IPutDashboardTypeInput;
    dataQuery: IDataQuery[];
    name: string;
    variables: IVariable[];
    config: IDataCardTypeConfig;
}

export interface IDataCardTypeConfig {
    displayType: string;
    displaySubType?: string;
}

export interface IPutDataCardTypeInput extends ISaveDataCardTypeInput {
    id: number;
}
