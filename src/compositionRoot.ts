import Persistence from './database/persistence';
import DashboardTypeManager from './managers/dashboardTypeManager';
import DataCardTypeManager from './managers/dataCardTypeManager';
import DataQueryOptionManager from './managers/dataQueryOptionManager';
import SubjectSelectionManager from './managers/subjectSelectionManager/subjectSelectionManager';
import RestService from './restService';
import DashboardTypeController from './restService/controllers/dashboardTypeController';
import DataCardTypeController from './restService/controllers/dataCardTypeController';
import DataQueryOptionController from './restService/controllers/dataQueryOptionController';
import SettingsController from './restService/controllers/settingsController';
import SubjectSelectionController from './restService/controllers/subjectSelectionController';
import RequestHandlersManager from './restService/requestHandlersManager';
import ErrorHandler from './restService/requestHandlersManager/handlers/errorHandler';
import { ErrorHandlerHelperFactory } from './restService/requestHandlersManager/handlers/errorHandler/errorHandlerHelper';
import AuthorizationHandler from './restService/requestHandlersManager/handlers/requestHandlers/authorizationHandler';
import BodyParseHandler from './restService/requestHandlersManager/handlers/requestHandlers/bodyParseHandler';
import NotFoundHandler from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler';
import Server from './server';
import Logger from './tools/logger';
import RequestValidator from './tools/requestValidator/requestValidator';
import SchemaGenerator from './tools/schemaGenerator';
import SchemaValidator from './tools/schemaValidator';
import SecretsManger from './tools/secretsManager';
import TokenManager from './tools/tokenManager';
import { IConfig } from './types';

class CompositionRoot {
    public static async createServer(config: IConfig): Promise<Server> {
        const logger = new Logger(config.logger);
        const secretsManager = new SecretsManger(logger);

        const persistence = new Persistence(config.persistence, secretsManager);
        await persistence.init();

        const tokenManager = new TokenManager(secretsManager);
        const schemaGenerator = new SchemaGenerator();
        const schemaValidator = new SchemaValidator();
        const requestValidator = new RequestValidator(schemaGenerator, schemaValidator);

        const errorHandlerHelperFactory = new ErrorHandlerHelperFactory(logger);

        const bodyParseHandler = new BodyParseHandler();
        const notFoundHandler = new NotFoundHandler();
        const authorizationHandler = new AuthorizationHandler(tokenManager);
        const errorHandler = new ErrorHandler(errorHandlerHelperFactory);
        const requestHandlerManager = new RequestHandlersManager(
            bodyParseHandler,
            notFoundHandler,
            authorizationHandler,
            errorHandler,
        );

        const subjectSelectionManager = new SubjectSelectionManager();
        const dashboardTypeManager = new DashboardTypeManager();
        const dataQueryOptionManager = new DataQueryOptionManager();
        const dataCardTypeManager = new DataCardTypeManager();
        const dashboardTypeController = new DashboardTypeController(
            requestHandlerManager,
            requestValidator,
            dashboardTypeManager,
        );
        const subjectSelectionController = new SubjectSelectionController(
            requestHandlerManager,
            requestValidator,
            subjectSelectionManager,
        );
        const dataQueryOptionController = new DataQueryOptionController(
            requestHandlerManager,
            requestValidator,
            dataQueryOptionManager,
        );
        const settingsController = new SettingsController(
            requestHandlerManager,
            requestValidator,
            dashboardTypeManager,
        );
        const dataCardTypeController = new DataCardTypeController(
            requestHandlerManager,
            requestValidator,
            dataCardTypeManager,
        );

        const restService = new RestService(
            config.server,
            logger,
            requestHandlerManager,
            [
                dashboardTypeController,
                subjectSelectionController,
                dataQueryOptionController,
                settingsController,
                dataCardTypeController,
            ],
        );

        return new Server(restService);
    }
}

export default CompositionRoot;
