import { ErrorRequestHandler } from 'express';

export interface IErrorHandler {
    getHandler(): ErrorRequestHandler;
}
