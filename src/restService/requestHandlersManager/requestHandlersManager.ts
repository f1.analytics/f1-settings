import { ErrorRequestHandler, RequestHandler } from 'express';

import ErrorHandler from './handlers/errorHandler';
import { IAuthorizationHandler } from './handlers/requestHandlers/authorizationHandler';
import BodyParseHandler from './handlers/requestHandlers/bodyParseHandler/bodyParseHandler';
import NotFoundHandler from './handlers/requestHandlers/notFoundHandler/notFoundHandler';
import { IRequestHandlerManager } from './requestHandlersManager.types';

class RequestHandlersManager implements IRequestHandlerManager {
    private readonly bodyParseHandler: BodyParseHandler;
    private readonly notFoundHandler: NotFoundHandler;
    private readonly authorizationHandler: IAuthorizationHandler;
    private readonly errorHandler: ErrorHandler;

    public constructor(
        bodyParseHandler: BodyParseHandler,
        notFoundHandler: NotFoundHandler,
        authorizationHandler: IAuthorizationHandler,
        errorHandler: ErrorHandler,
    ) {
        this.bodyParseHandler = bodyParseHandler;
        this.notFoundHandler = notFoundHandler;
        this.authorizationHandler = authorizationHandler;
        this.errorHandler = errorHandler;
    }

    public getBodyParseHandler(): RequestHandler {
        return this.bodyParseHandler.getHandler();
    }

    public getNotFoundHandler(): RequestHandler {
        return this.notFoundHandler.getHandler();
    }

    public getAuthorizationHandler(): RequestHandler {
        return this.authorizationHandler.getHandler();
    }

    public getErrorHandler(): ErrorRequestHandler {
        return this.errorHandler.getHandler();
    }
}

export default RequestHandlersManager;
