import RestService from './restService';

export * from './restService.types';
export default RestService;
