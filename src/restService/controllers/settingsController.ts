import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { IDashboardTypeManager } from '../../managers/dashboardTypeManager';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class SettingsController extends Controller {
    private readonly dashboardTypeManager: IDashboardTypeManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dashboardTypeManager: IDashboardTypeManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dashboardTypeManager = dashboardTypeManager;
        this.route = '/';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.get('/', this.getSettings());

        return router;
    }

    private getSettings(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardTypes = await this.dashboardTypeManager.getDashboardTypes();
                res.send({
                    dashboardTypes,
                });
            } catch (error) {
                next(error);
            }
        };
    }
}

export default SettingsController;
