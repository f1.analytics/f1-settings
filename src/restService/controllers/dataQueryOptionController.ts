import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { DataQueryOptionNotFoundError } from '../../errors/requestErrors';
import { IDataQueryOptionManager } from '../../managers/dataQueryOptionManager';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IPutDataQueryOptionInput } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class DataQueryOptionController extends Controller {
    private readonly dataQueryOptionManager: IDataQueryOptionManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dataQueryOptionManager: IDataQueryOptionManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dataQueryOptionManager = dataQueryOptionManager;
        this.route = '/dataQueryOptions';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.use(this.requestHandlerManager.getAuthorizationHandler());

        router.get('/', this.getDataQueryOptions());
        router.get('/:id', this.getDataQueryOption());
        router.post('/', this.postDataQueryOption());
        router.put('/', this.putDataQueryOptionHandler());
        router.delete('/:id', this.deleteDataQueryOptionHandler());

        return router;
    }

    private getDataQueryOptions(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptions = await this.dataQueryOptionManager.getDataQueryOptions();
                res.send(dataQueryOptions);
            } catch (error) {
                next(error);
            }
        };
    }

    private getDataQueryOption(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptionId = this.requestValidator.validateNumberParam(req.params, 'id');
                const dataQueryOption = await this.dataQueryOptionManager.getDataQueryOption(dataQueryOptionId);
                res.send(dataQueryOption);
            } catch (error) {
                next(error);
            }
        };
    }

    private postDataQueryOption(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOption = req.body;
                this.requestValidator.validateRequestData(dataQueryOption, 'ISaveDataQueryOptionInput');
                const id = await this.dataQueryOptionManager.createDataQueryOption(dataQueryOption);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private putDataQueryOptionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptionInput = req.body;
                this.requestValidator.validateRequestData(dataQueryOptionInput, 'IPutDataQueryOptionInput');
                await this.updateOrCreateDataQueryOption(dataQueryOptionInput);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDataQueryOptionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptionId = this.requestValidator.validateNumberParam(req.params, 'id');
                await this.dataQueryOptionManager.deleteDataQueryOption(dataQueryOptionId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private async updateOrCreateDataQueryOption(dataQueryOptionInput: IPutDataQueryOptionInput): Promise<void> {
        try {
            await this.dataQueryOptionManager.getDataQueryOption(dataQueryOptionInput.id);
            await this.dataQueryOptionManager.updateDataQueryOption(dataQueryOptionInput);
        } catch (error) {
            if (error instanceof DataQueryOptionNotFoundError) {
                await this.dataQueryOptionManager.createDataQueryOption(dataQueryOptionInput);
            } else {
                throw error;
            }
        }
    }
}

export default DataQueryOptionController;
