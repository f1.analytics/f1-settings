import { Router } from 'express';

import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import { IController } from './controller.types';

abstract class Controller implements IController {
    protected readonly requestHandlerManager: IRequestHandlerManager;
    protected readonly requestValidator: IRequestValidator;
    protected route: string;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
    ) {
        this.requestHandlerManager = requestHandlerManager;
        this.requestValidator = requestValidator;
    }

    public abstract resolveRouter(): Router;

    public getRoute(): string {
        return this.route;
    }
}

export default Controller;
