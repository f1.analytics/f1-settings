import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { DataCardTypeNotFoundError } from '../../errors/requestErrors';
import { IDataCardTypeManager } from '../../managers/dataCardTypeManager/dataCardTypeManager.types';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IPutDataCardTypeInput, ISaveDataCardTypeInput } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class DataCardTypeController extends Controller {
    private readonly dataCardTypeManager: IDataCardTypeManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dataCardTypeManager: IDataCardTypeManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dataCardTypeManager = dataCardTypeManager;
        this.route = '/dataCardTypes';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.use(this.requestHandlerManager.getAuthorizationHandler());

        router.get('/', this.getDataCardTypesHandler());
        router.get('/:id', this.getDataCardTypeHandler());
        router.post('/', this.postDataCardTypeHandler());
        router.put('/', this.putDataCardTypeHandler());
        router.delete('/:id', this.deleteDataCardTypeHandler());

        return router;
    }

    private getDataCardTypesHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCardTypes = await this.dataCardTypeManager.getDataCardTypes();
                res.send(dataCardTypes);
            } catch (error) {
                next(error);
            }
        };
    }

    private getDataCardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCardTypeId = this.requestValidator.validateNumberParam(req.params, 'id');
                const dataCardType = await this.dataCardTypeManager.getDataCardType(dataCardTypeId);
                res.send(dataCardType);
            } catch (error) {
                next(error);
            }
        };
    }

    private postDataCardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCardType = req.body;
                this.requestValidator.validateRequestData(dataCardType, 'ISaveDataCardTypeInput');
                const id = await this.dataCardTypeManager.createDataCardType(dataCardType);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private putDataCardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCardType = req.body;
                this.requestValidator.validateRequestData(dataCardType, 'IPutDataCardTypeInput');
                await this.updateOrCreateDataCardType(dataCardType);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDataCardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataCardTypeId = this.requestValidator.validateNumberParam(req.params, 'id');
                await this.dataCardTypeManager.deleteDataCardType(dataCardTypeId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private async updateOrCreateDataCardType(dataCardTypeInput: IPutDataCardTypeInput): Promise<void> {
        try {
            await this.dataCardTypeManager.getDataCardType(dataCardTypeInput.id);
            await this.dataCardTypeManager.updateDataCardType(dataCardTypeInput);
        } catch (error) {
            if (error instanceof DataCardTypeNotFoundError) {
                await this.dataCardTypeManager.createDataCardType(dataCardTypeInput);
            } else {
                throw error;
            }
        }
    }
}

export default DataCardTypeController;
