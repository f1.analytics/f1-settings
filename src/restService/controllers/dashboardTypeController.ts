import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { DashboardTypeNotFoundError } from '../../errors';
import { IDashboardTypeManager } from '../../managers/dashboardTypeManager';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IPutDashboardTypeInput } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class DashboardTypeController extends Controller {
    private readonly dashboardTypeManager: IDashboardTypeManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        dashboardTypeManager: IDashboardTypeManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.dashboardTypeManager = dashboardTypeManager;
        this.route = '/dashboardTypes';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.use(this.requestHandlerManager.getAuthorizationHandler());

        router.get('/', this.getDashboardTypesHandler());
        router.get('/:id', this.getDashboardTypeHandler());
        router.post('/', this.postDashboardTypeHandler());
        router.put('/', this.putDashboardTypeHandler());
        router.delete('/:id', this.deleteDashboardTypeHandler());

        return router;
    }

    private getDashboardTypesHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardTypes = await this.dashboardTypeManager.getDashboardTypes();
                res.send(dashboardTypes);
            } catch (error) {
                next(error);
            }
        };
    }

    private getDashboardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardId = this.requestValidator.validateNumberParam(req.params, 'id');
                const dashboardType = await this.dashboardTypeManager.getDashboardType(dashboardId);
                res.send(dashboardType);
            } catch (error) {
                next(error);
            }
        };
    }

    private postDashboardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardTypeInput = req.body;
                this.requestValidator.validateRequestData(dashboardTypeInput, 'ISaveDashboardTypeInput');
                const id = await this.dashboardTypeManager.saveDashboardType(dashboardTypeInput);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private putDashboardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardTypeInput = req.body;
                this.requestValidator.validateRequestData(dashboardTypeInput, 'IPutDashboardTypeInput');
                await this.replaceOrCreateDashboardType(dashboardTypeInput);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteDashboardTypeHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dashboardTypeId = this.requestValidator.validateNumberParam(req.params, 'id');
                await this.dashboardTypeManager.deleteDashboardType(dashboardTypeId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private async replaceOrCreateDashboardType(dashboardTypeInput: IPutDashboardTypeInput): Promise<void> {
        try {
            await this.dashboardTypeManager.getDashboardType(dashboardTypeInput.id);
            await this.dashboardTypeManager.updateDashboardType(dashboardTypeInput);
        } catch (error) {
            if (error instanceof DashboardTypeNotFoundError) {
                await this.dashboardTypeManager.saveDashboardType(dashboardTypeInput);
            } else {
                throw error;
            }
        }
    }
}

export default DashboardTypeController;
