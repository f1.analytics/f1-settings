import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { SubjectSelectionNotFoundError } from '../../errors/requestErrors';
import { ISubjectSelectionManager } from '../../managers/subjectSelectionManager/subjectSelectionManager.type';
import { IRequestValidator } from '../../tools/requestValidator/requestValidator.types';
import { IPutSubjectSelectionInput, ISaveSubjectSelectionInput } from '../../types';
import { IRequestHandlerManager } from '../requestHandlersManager';
import Controller from './controller';

class SubjectSelectionController extends Controller {
    private readonly subjectSelectionManager: ISubjectSelectionManager;

    public constructor(
        requestHandlerManager: IRequestHandlerManager,
        requestValidator: IRequestValidator,
        subjectSelectionManager: ISubjectSelectionManager,
    ) {
        super(requestHandlerManager, requestValidator);
        this.subjectSelectionManager = subjectSelectionManager;
        this.route = '/subjectSelections';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.use(this.requestHandlerManager.getAuthorizationHandler());

        router.get('/', this.getSubjectSelectionsHandler());
        router.get('/:id', this.getSubjectSelectionHandler());
        router.post('/', this.postSubjectSelectionHandler());
        router.put('/', this.putSubjectSelectionHandler());
        router.delete('/:id', this.deleteSubjectSelectionHandler());

        return router;
    }

    private getSubjectSelectionsHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelections = await this.subjectSelectionManager.getSubjectSelections();
                res.send(subjectSelections);
            } catch (error) {
                next(error);
            }
        };
    }

    private getSubjectSelectionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelectionId = this.requestValidator.validateNumberParam(req.params, 'id');
                const subjectSelection = await this.subjectSelectionManager.getSubjectSelection(subjectSelectionId);
                res.send(subjectSelection);
            } catch (error) {
                next(error);
            }
        };
    }

    private postSubjectSelectionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelection = req.body;
                this.requestValidator.validateRequestData(subjectSelection, 'ISaveSubjectSelectionInput');
                const id = await this.subjectSelectionManager.createSubjectSelection(subjectSelection);
                res.send({ id });
            } catch (error) {
                next(error);
            }
        };
    }

    private putSubjectSelectionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelection = req.body;
                this.requestValidator.validateRequestData(subjectSelection, 'IPutSubjectSelectionInput');
                await this.updateOrCreateSubjectSelection(subjectSelection);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private deleteSubjectSelectionHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const subjectSelectionId = this.requestValidator.validateNumberParam(req.params, 'id');
                await this.subjectSelectionManager.deleteSubjectSelection(subjectSelectionId);
                res.send();
            } catch (error) {
                next(error);
            }
        };
    }

    private async updateOrCreateSubjectSelection(subjectSelectionInput: IPutSubjectSelectionInput): Promise<void> {
        try {
            await this.subjectSelectionManager.getSubjectSelection(subjectSelectionInput.id);
            await this.subjectSelectionManager.updateSubjectSelection(subjectSelectionInput);
        } catch (error) {
            if (error instanceof SubjectSelectionNotFoundError) {
                await this.subjectSelectionManager.createSubjectSelection(subjectSelectionInput);
            } else {
                throw error;
            }
        }
    }
}

export default SubjectSelectionController;
