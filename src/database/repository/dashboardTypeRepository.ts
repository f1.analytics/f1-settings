import { EntityRepository, Repository } from 'typeorm';

import { IPutDashboardTypeInput, ISaveDashboardTypeInput } from '../../types';
import DashboardType from '../entity/dashboardType';
import { relations } from './repository.types';

@EntityRepository(DashboardType)
class DashboardTypeRepository extends Repository<DashboardType> {
    public getDashboardTypes(): Promise<DashboardType[]> {
        return this.find();
    }

    public getDashboardType(id: number): Promise<DashboardType> {
        return this.findOneOrFail({
            relations: [relations.SubjectSelections, relations.DataCardTypes],
            where: {
                id,
            },
        });
    }

    public async saveDashboardType(dashboardTypeInput: ISaveDashboardTypeInput): Promise<DashboardType> {
        const dashboardType = await this.create(dashboardTypeInput);
        return this.save(dashboardType);
    }

    public async updateDashboardType(dashboardTypeInput: IPutDashboardTypeInput): Promise<void> {
        await this.update(dashboardTypeInput.id, dashboardTypeInput);
    }

    public async deleteDashboardType(id: number): Promise<void> {
        await this.delete(id);
    }
}

export default DashboardTypeRepository;
