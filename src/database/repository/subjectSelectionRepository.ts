import { EntityRepository, Repository } from 'typeorm';

import { IPutSubjectSelectionInput, ISaveSubjectSelectionInput } from '../../types';
import SubjectSelection from '../entity/subjectSelection';
import { relations } from './repository.types';

@EntityRepository(SubjectSelection)
class SubjectSelectionRepository extends Repository<SubjectSelection> {
    public getSubjectSelections(): Promise<SubjectSelection[]> {
        return this.find();
    }

    public getSubjectSelection(id: number): Promise<SubjectSelection> {
        return this.findOneOrFail({
            relations: [relations.DashboardType],
            where: {
                id,
            },
        });
    }

    public async saveSubjectSelection(subjectSelectionInput: ISaveSubjectSelectionInput): Promise<SubjectSelection> {
        const subjectSelection = this.create(subjectSelectionInput);
        return this.save(subjectSelection);
    }

    public async updateSubjectSelection(subjectSelectionInput: IPutSubjectSelectionInput): Promise<void> {
        const subjectSelection = this.getSubjectSelection(subjectSelectionInput.id);
        await this.save({
            ...subjectSelection,
            ...subjectSelectionInput,
        });
    }

    public async deleteSubjectSelection(id: number): Promise<void> {
        await this.delete(id);
    }
}

export default SubjectSelectionRepository;
