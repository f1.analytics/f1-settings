import { EntityRepository, Repository } from 'typeorm';

import { IPutDataQueryOptionInput, ISaveDataQueryOptionInput } from '../../types';
import DataQueryOption from '../entity/dataQueryOption';
import { relations } from './repository.types';

@EntityRepository(DataQueryOption)
class DataQueryOptionRepository extends Repository<DataQueryOption> {
    public getDataQueryOptions(): Promise<DataQueryOption[]> {
        return this.find();
    }

    public getDataQueryOption(id: number): Promise<DataQueryOption> {
        return this.findOneOrFail({
            relations: [relations.DataQueryOptionVariables],
            where: {
                id,
            },
        });
    }

    public async createDataQueryOption(dataQueryOptionInput: ISaveDataQueryOptionInput): Promise<DataQueryOption> {
        const dataQueryOption = await this.create(dataQueryOptionInput);
        return this.save(dataQueryOption);
    }

    public async updateDataQueryOption(dataQueryOptionInput: IPutDataQueryOptionInput): Promise<void> {
        const dataQueryOption = await this.getDataQueryOption(dataQueryOptionInput.id);
        await this.save({
            ...dataQueryOption,
            ...dataQueryOptionInput,
        });
    }

    public async deleteDataQueryOption(id: number): Promise<void> {
        await this.delete(id);
    }
}

export default DataQueryOptionRepository;
