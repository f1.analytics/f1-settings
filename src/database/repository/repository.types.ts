export enum relations {
    DashboardType = 'dashboardType',
    DataQueryOptionVariables = 'variables',
    SubjectSelections = 'subjectSelections',
    DataCardTypes = 'dataCardTypes',
}
