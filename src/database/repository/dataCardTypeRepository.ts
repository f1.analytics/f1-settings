import { EntityRepository, Repository } from 'typeorm';

import { IPutDataCardTypeInput, ISaveDataCardTypeInput } from '../../types';
import DataCardType from '../entity/dataCardType';
import { relations } from './repository.types';

@EntityRepository(DataCardType)
class DataCardTypeRepository extends Repository<DataCardType> {
    public getDataCardTypes(): Promise<DataCardType[]> {
        return this.find();
    }

    public getDataCardType(id: number): Promise<DataCardType> {
        return this.findOneOrFail({
            relations: [relations.DashboardType],
            where: {
                id,
            },
        });
    }

    public async saveDataCardType(dataCardTypeInput: ISaveDataCardTypeInput): Promise<DataCardType> {
        const dataCardType = this.create(dataCardTypeInput);
        return this.save(dataCardType);
    }

    public async updateDataCardType(dataCardTypeInput: IPutDataCardTypeInput): Promise<void> {
        const dataCardType = this.getDataCardType(dataCardTypeInput.id);
        await this.save({
            ...dataCardType,
            ...dataCardTypeInput,
        });
    }

    public async deleteDataCardType(id: number): Promise<void> {
        await this.delete(id);
    }
}

export default DataCardTypeRepository;
