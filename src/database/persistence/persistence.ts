import { createConnection } from 'typeorm';

import { ISecretsManager } from '../../tools/secretsManager';
import { IPersistenceConfig } from '../../types';
import { IPersistence } from './persistence.types';

class Persistence implements IPersistence {
    private readonly config: IPersistenceConfig;
    private readonly secretsManager: ISecretsManager;

    public constructor(config: IPersistenceConfig, secretsManager: ISecretsManager) {
        this.config = config;
        this.secretsManager = secretsManager;
    }

    public async init(): Promise<void> {
        // @ts-ignore
        await createConnection({
            entities: [
                __dirname + '/../entity/*.js',
                __dirname + '/../entity/*.ts',
            ],
            password: this.secretsManager.get('MYSQL_PASSWORD'),
            synchronize: true,
            username: this.secretsManager.get('MYSQL_USER'),
            ...this.config,
        });
    }
}

export default Persistence;
