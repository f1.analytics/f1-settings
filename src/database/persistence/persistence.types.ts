export interface IPersistence {
    init(): Promise<void>;
}

export enum PersistenceErrorTypes {
    entityNotFound = 'EntityNotFound',
}
