import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { IDataCardTypeConfig, IDataQuery, IVariable } from '../../types';
import DashboardType from './dashboardType';

@Entity()
class DataCardType {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column('text')
    public name: string;

    @Column('simple-json')
    public dataQuery: IDataQuery[];

    @Column('simple-json')
    public variables: IVariable[];

    @Column('simple-json')
    public config: IDataCardTypeConfig;

    @CreateDateColumn()
    public createDate: string;

    @ManyToOne(
        () => DashboardType,
        (dashboardType) => dashboardType.subjectSelections,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'dashboardTypeId' })
    public dashboardType: DashboardType;
}

export default DataCardType;
