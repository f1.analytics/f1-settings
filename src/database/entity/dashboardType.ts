import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import DataCardType from './dataCardType';
import SubjectSelection from './subjectSelection';

@Entity()
class DashboardType {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column('text')
    public name: string;

    @Column('text')
    public icon: string;

    @OneToMany(
        () => SubjectSelection,
        (subjectSelection) => subjectSelection.dashboardType,
        { cascade: true },
    )
    public subjectSelections: SubjectSelection[];

    @OneToMany(
        () => DataCardType,
        (dataCardType) => dataCardType.dashboardType,
        { cascade: true },
    )
    public dataCardTypes: DataCardType[];
}

export default DashboardType;
