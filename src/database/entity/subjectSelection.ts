import { Column, CreateDateColumn, Entity, Generated, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ISubjectSelectionConfig } from '../../types';
import DashboardType from './dashboardType';

@Entity()
class SubjectSelection {
    @PrimaryGeneratedColumn()
    public id: number;

    @Generated('increment')
    public sorting: number;

    @Column('text')
    public name: string;

    @Column('simple-json')
    public config: ISubjectSelectionConfig;

    @CreateDateColumn()
    public createDate: string;

    @ManyToOne(
        () => DashboardType,
        (dashboardType) => dashboardType.subjectSelections,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'dashboardTypeId' })
    public dashboardType: DashboardType;
}

export default SubjectSelection;
