import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { DataQueryVariableTypes } from '../../types';
import DataQueryOption from './dataQueryOption';

@Entity()
class DataQueryOptionVariable {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column('text')
    public name: string;

    @Column('text')
    public type: DataQueryVariableTypes;

    @Column('simple-array', {
        nullable: true,
    })
    public availableValues: Array<string | number>;

    @Column('text')
    public exampleValue: string | number;

    @ManyToOne(
        () => DataQueryOption,
        (dataQueryOption) => dataQueryOption.variables,
        { onDelete: 'CASCADE' },
    )
    @JoinColumn({ name: 'dataQueryOptionId' })
    public dataQueryOption: DataQueryOption;
}

export default DataQueryOptionVariable;
