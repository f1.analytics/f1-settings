import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import DataQueryOptionVariable from './dataQueryOptionVariable';

@Entity()
class DataQueryOption {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column('text')
    public name: string;

    @Column('text')
    public script: string;

    @OneToMany(
        () => DataQueryOptionVariable,
        (variable) => variable.dataQueryOption,
        { cascade: true },
    )
    public variables: DataQueryOptionVariable[];
}

export default DataQueryOption;
