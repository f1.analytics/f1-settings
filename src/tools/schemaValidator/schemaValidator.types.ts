export interface ISchemaValidator {
    validate(schema: object, data: object): void;
}
