export const mockLoggerConfig = {
    level: 'debug',
};

export const mockMessage = 'Test 123';
export const mockError = { name: 'Error', message: 'This error occurred' };
export const mockRequest = { body: { id: 123 }, method: 'GET', originalUrl: '/dashboards' };

export const expectedWarnArgs = {
    message: 'Test 123:\nGET /dashboards, {\"id\":123}',
    meta: { error: mockError },
};
