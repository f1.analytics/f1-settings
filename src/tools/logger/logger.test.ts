import Logger from './logger';
import { expectedWarnArgs, mockError, mockLoggerConfig, mockMessage, mockRequest } from './logger.test.data';

describe('Logger tests', () => {
    const logger = new Logger(mockLoggerConfig);
    const winstonLogger = logger.getWinstonLogger();

    it('Should log info', () => {
        const infoSpy = jest.spyOn(winstonLogger, 'info');
        logger.info(mockMessage);
        expect(infoSpy).toHaveBeenCalledWith(mockMessage);
    });

    it('Should log warnings', () => {
        const warnSpy = jest.spyOn(winstonLogger, 'warn');
        logger.warning(mockMessage, { error: mockError, req: (mockRequest as any) });
        expect(warnSpy).toHaveBeenCalledWith(expectedWarnArgs.message, expectedWarnArgs.meta);
    });

    it('Should log errors', () => {
        const errorSpy = jest.spyOn(winstonLogger, 'error');
        logger.error(mockMessage, { error: mockError, req: (mockRequest as any) });
        expect(errorSpy).toHaveBeenCalledWith(expectedWarnArgs.message, expectedWarnArgs.meta);
    });
});
