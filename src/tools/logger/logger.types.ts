import { Request } from 'express';

export interface ILogger {
    info(message: string): void;
    warning(message: string, meta?: ILoggerMeta): void;
    error(message: string, meta?: ILoggerMeta): void;
}

export interface ILoggerMeta {
    error?: Error;
    req?: Request;
}
