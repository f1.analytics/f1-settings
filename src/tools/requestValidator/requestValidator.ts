import { BadRequestError } from '../../errors/requestErrors';
import { ISchemaGenerator } from '../schemaGenerator';
import { ISchemaValidator } from '../schemaValidator';
import { IRequestValidator } from './requestValidator.types';

class RequestValidator implements IRequestValidator {
    private readonly schemaGenerator: ISchemaGenerator;
    private readonly schemaValidator: ISchemaValidator;

    public constructor(schemaGenerator: ISchemaGenerator, schemaValidator: ISchemaValidator) {
        this.schemaGenerator = schemaGenerator;
        this.schemaValidator = schemaValidator;
    }

    public validateRequestData(data: object, typeName: string): void {
        const schema = this.schemaGenerator.generate(typeName);
        this.schemaValidator.validate(schema, data);
    }

    public validateNumberParam(params: any, key: string): number {
        const value = Number(params[key]);
        if (Number.isNaN(value)) {
            throw new BadRequestError(`Invalid params key, ${key} should be a number, received value ${value}`);
        }
        return value;
    }
}

export default RequestValidator;
