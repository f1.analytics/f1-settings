export interface IRequestValidator {
    validateRequestData(data: object, typeName: string): void;
    validateNumberParam(params: any, key: string): number;
}
