import { BadRequestError } from '../../errors/requestErrors';
import RequestValidator from './requestValidator';
import {
    mockObject,
    mockSchema,
    mockSchemaGenerator,
    mockSchemaName,
    mockSchemaValidator,
} from './requestValidator.test.data';

describe('Request validator tests', () => {
    const requestValidator = new RequestValidator(mockSchemaGenerator, mockSchemaValidator);

    it('Should generate and validate schema', () => {
        requestValidator.validateRequestData(mockObject, mockSchemaName);
        expect(mockSchemaGenerator.generate).toHaveBeenCalledWith(mockSchemaName);
        expect(mockSchemaValidator.validate).toHaveBeenCalledWith(mockSchema, mockObject);
    });
    it('Should validate and return number param', () => {
        expect(requestValidator.validateNumberParam({ id: '2' }, 'id')).toBe(2);
    });
    it('Should throw if param number is invalid', () => {
        const actual = () => requestValidator.validateNumberParam({ id: 'undefined' }, 'id');
        expect(actual).toThrowError(BadRequestError);
    });
});
