import jwt from 'jsonwebtoken';

import { UnauthorizedError } from '../../errors/requestErrors';
import TokenManager from './tokenManager';

describe('Token manager tests', () => {
    const mockPublicKey = 'pubKey';
    const mockToken = 'token';
    const secretsManager = {
        get: () => mockPublicKey,
    };
    const tokenManager = new TokenManager(secretsManager);
    it('Should verify jwt', () => {
        const mockPayload = { test: 123 };
        const spy = jest.spyOn(jwt, 'verify').mockImplementation(() => mockPayload);
        expect(tokenManager.validateJwtToken(mockToken)).toEqual(mockPayload);
        expect(spy).toHaveBeenCalledWith(mockToken, mockPublicKey);
    });
    it('Should throw error if token not valid', () => {
        const mockError = new Error('invalidToken');
        jest.spyOn(jwt, 'verify').mockImplementation(() => {
            throw mockError;
        });
        expect(() => tokenManager.validateJwtToken(mockToken)).toThrow(UnauthorizedError);
    });
});
