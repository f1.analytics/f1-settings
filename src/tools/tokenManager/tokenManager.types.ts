export interface ITokenManager {
    validateJwtToken(token: string): any;
    validateStaticToken(token: string): void;
}
