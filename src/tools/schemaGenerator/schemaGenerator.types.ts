import { Definition } from 'typescript-json-schema';

export interface ISchemaGenerator {
    generate(schemaName: string): Definition;
}
