import { IError } from './errors.types';

abstract class BaseError extends Error implements IError {
    public constructor(...args: any) {
        super(...args);
        Error.captureStackTrace(this, BaseError);
    }

    public getMessage(): string {
        return this.message;
    }
}

export default BaseError;
