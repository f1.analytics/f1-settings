import BaseError from '../baseError';
import { errorCodes, statusCodes } from '../errors.types';
import { IRequestError } from './requestErrors.types';

export abstract class RequestError extends BaseError implements IRequestError {
    protected statusCode: number = statusCodes.INTERNAL_SERVER_ERROR;
    protected errorCode: string = errorCodes.INTERNAL_SERVER_ERROR;

    public getStatusCode(): number {
        return this.statusCode;
    }

    public getErrorCode(): string {
        return this.errorCode;
    }
}
