import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class DashboardTypeNotFoundError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.NOT_FOUND;
        this.errorCode = errorCodes.DASHBOARD_TYPE_NOT_FOUND;
    }
}
