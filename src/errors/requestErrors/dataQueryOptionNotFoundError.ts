import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class DataQueryOptionNotFoundError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.NOT_FOUND;
        this.errorCode = errorCodes.DATA_QUERY_OPTION_NOT_FOUND;
    }
}
