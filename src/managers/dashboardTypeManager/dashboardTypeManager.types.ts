import DashboardType from '../../database/entity/dashboardType';
import { IPutDashboardTypeInput, ISaveDashboardTypeInput } from '../../types';

export interface IDashboardTypeManager {
    getDashboardTypes(): Promise<DashboardType[]>;
    getDashboardType(id: number): Promise<DashboardType>;
    saveDashboardType(dashboardTypeInput: ISaveDashboardTypeInput): Promise<number>;
    updateDashboardType(dashboardTypeInput: IPutDashboardTypeInput): Promise<void>;
    deleteDashboardType(id: number): Promise<void>;
}
