import { getCustomRepository } from 'typeorm';

import DashboardType from '../../database/entity/dashboardType';
import { PersistenceErrorTypes } from '../../database/persistence';
import DashboardTypeRepository from '../../database/repository/dashboardTypeRepository';
import { DashboardTypeNotFoundError } from '../../errors/requestErrors';
import { IPutDashboardTypeInput, ISaveDashboardTypeInput } from '../../types';
import { IDashboardTypeManager } from './dashboardTypeManager.types';

class DashboardTypeManager implements IDashboardTypeManager {
    private readonly repository: DashboardTypeRepository;

    public constructor() {
        this.repository = getCustomRepository(DashboardTypeRepository);
    }

    public getDashboardTypes(): Promise<DashboardType[]> {
        return this.repository.getDashboardTypes();
    }

    public async getDashboardType(id: number): Promise<DashboardType> {
        try {
            return await this.repository.getDashboardType(id);
        } catch (error) {
            if (error.name === PersistenceErrorTypes.entityNotFound) {
                throw new DashboardTypeNotFoundError(`Dashboard type ${id} not found`);
            } else {
                throw error;
            }
        }
    }

    public async saveDashboardType(dashboardTypeInput: ISaveDashboardTypeInput): Promise<number> {
        const result = await this.repository.saveDashboardType(dashboardTypeInput);
        return result.id;
    }

    public async updateDashboardType(dashboardInput: IPutDashboardTypeInput): Promise<void> {
        await this.repository.updateDashboardType(dashboardInput);
    }

    public async deleteDashboardType(id: number): Promise<void> {
        await this.getDashboardType(id);
        await this.repository.deleteDashboardType(id);
    }
}

export default DashboardTypeManager;
