import DashboardTypeManager from './dashboardTypeManager';

export * from './dashboardTypeManager.types';
export default DashboardTypeManager;
