import DataCardType from '../../database/entity/dataCardType';
import { IPutDataCardTypeInput, ISaveDataCardTypeInput } from '../../types';

export interface IDataCardTypeManager {
    getDataCardTypes(): Promise<DataCardType[]>;
    getDataCardType(id: number): Promise<DataCardType>;
    createDataCardType(dataCardTypeInput: ISaveDataCardTypeInput): Promise<number>;
    updateDataCardType(dataCardTypeInput: IPutDataCardTypeInput): Promise<void>;
    deleteDataCardType(id: number): Promise<void>;
}
