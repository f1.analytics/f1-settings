import { getCustomRepository } from 'typeorm';

import DataCardType from '../../database/entity/dataCardType';
import { PersistenceErrorTypes } from '../../database/persistence';
import DataCardTypeRepository from '../../database/repository/dataCardTypeRepository';
import { DataCardTypeNotFoundError } from '../../errors/requestErrors';
import { IPutDataCardTypeInput, ISaveDataCardTypeInput } from '../../types';
import { IDataCardTypeManager } from './dataCardTypeManager.types';

class DataCardTypeManager implements IDataCardTypeManager {
    private readonly repository: DataCardTypeRepository;

    public constructor() {
        this.repository = getCustomRepository(DataCardTypeRepository);
    }

    public async getDataCardTypes(): Promise<DataCardType[]> {
        return await this.repository.getDataCardTypes();
    }

    public async getDataCardType(id: number): Promise<DataCardType> {
        try {
            return await this.repository.getDataCardType(id);
        } catch (error) {
            if (error.name === PersistenceErrorTypes.entityNotFound) {
                throw new DataCardTypeNotFoundError(`Data card type ${id} not found`);
            } else {
                throw error;
            }
        }
    }

    public async createDataCardType(dataCardTypeInput: ISaveDataCardTypeInput): Promise<number> {
        const result = await this.repository.saveDataCardType(dataCardTypeInput);
        return result.id;
    }

    public updateDataCardType(dataCardTypeInput: IPutDataCardTypeInput): Promise<void> {
        return this.repository.updateDataCardType(dataCardTypeInput);
    }

    public async deleteDataCardType(id: number): Promise<void> {
        await this.getDataCardType(id);
        await this.repository.deleteDataCardType(id);
    }
}

export default DataCardTypeManager;
