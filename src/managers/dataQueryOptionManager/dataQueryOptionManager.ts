import { getCustomRepository } from 'typeorm';

import DataQueryOption from '../../database/entity/dataQueryOption';
import { PersistenceErrorTypes } from '../../database/persistence';
import DataQueryOptionRepository from '../../database/repository/dataQueryOptionRepository';
import { DataQueryOptionNotFoundError } from '../../errors/requestErrors';
import { IPutDataQueryOptionInput, ISaveDataQueryOptionInput } from '../../types';
import { IDataQueryOptionManager } from './dataQueryOptionManager.types';

class DataQueryOptionManager implements IDataQueryOptionManager {
    private readonly repository: DataQueryOptionRepository;

    public constructor() {
        this.repository = getCustomRepository(DataQueryOptionRepository);
    }

    public getDataQueryOptions(): Promise<DataQueryOption[]> {
        return this.repository.getDataQueryOptions();
    }

    public async getDataQueryOption(id: number): Promise<DataQueryOption> {
        try {
            return await this.repository.getDataQueryOption(id);
        } catch (error) {
            if (error.name === PersistenceErrorTypes.entityNotFound) {
                throw new DataQueryOptionNotFoundError(`Data query option ${id} not found`);
            } else {
                throw error;
            }
        }
    }

    public async createDataQueryOption(dataQueryOptionInput: ISaveDataQueryOptionInput): Promise<number> {
        const { id } = await this.repository.createDataQueryOption(dataQueryOptionInput);
        return id;
    }

    public updateDataQueryOption(dataQueryOptionInput: IPutDataQueryOptionInput): Promise<void> {
        return this.repository.updateDataQueryOption(dataQueryOptionInput);
    }

    public async deleteDataQueryOption(id: number): Promise<void> {
        await this.getDataQueryOption(id);
        await this.repository.deleteDataQueryOption(id);
    }
}

export default DataQueryOptionManager;
