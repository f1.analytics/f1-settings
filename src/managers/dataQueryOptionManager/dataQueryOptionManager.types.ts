import DataQueryOption from '../../database/entity/dataQueryOption';
import { IPutDataQueryOptionInput, ISaveDataQueryOptionInput } from '../../types';

export interface IDataQueryOptionManager {
    getDataQueryOptions(): Promise<DataQueryOption[]>;
    getDataQueryOption(id: number): Promise<DataQueryOption>;
    createDataQueryOption(dataQueryOptionInput: ISaveDataQueryOptionInput): Promise<number>;
    updateDataQueryOption(dataQueryOptionInput: IPutDataQueryOptionInput): Promise<void>;
    deleteDataQueryOption(id: number): Promise<void>;
}
