import SubjectSelection from '../../database/entity/subjectSelection';
import {
    IPutSubjectSelectionInput,
    ISaveSubjectSelectionInput,
} from '../../types';

export interface ISubjectSelectionManager {
    getSubjectSelections(): Promise<SubjectSelection[]>;
    getSubjectSelection(id: number): Promise<SubjectSelection>;
    createSubjectSelection(subjectSelectionInput: ISaveSubjectSelectionInput): Promise<number>;
    updateSubjectSelection(subjectSelectionInput: IPutSubjectSelectionInput): Promise<void>;
    deleteSubjectSelection(id: number): Promise<void>;
}
