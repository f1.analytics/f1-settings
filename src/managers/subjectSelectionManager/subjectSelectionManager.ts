import { getCustomRepository } from 'typeorm';

import SubjectSelection from '../../database/entity/subjectSelection';
import { PersistenceErrorTypes } from '../../database/persistence';
import SubjectSelectionRepository from '../../database/repository/subjectSelectionRepository';
import { SubjectSelectionNotFoundError } from '../../errors/requestErrors';
import { IPutSubjectSelectionInput, ISaveSubjectSelectionInput } from '../../types';
import { ISubjectSelectionManager } from './subjectSelectionManager.type';

class SubjectSelectionManager implements ISubjectSelectionManager {
    private readonly repository: SubjectSelectionRepository;

    public constructor() {
        this.repository = getCustomRepository(SubjectSelectionRepository);
    }

    public async getSubjectSelections(): Promise<SubjectSelection[]> {
        return await this.repository.getSubjectSelections();
    }

    public async getSubjectSelection(id: number): Promise<SubjectSelection> {
        try {
            return await this.repository.getSubjectSelection(id);
        } catch (error) {
            if (error.name === PersistenceErrorTypes.entityNotFound) {
                throw new SubjectSelectionNotFoundError(`Subject selection ${id} not found`);
            } else {
                throw error;
            }
        }
    }

    public async createSubjectSelection(subjectSelectionInput: ISaveSubjectSelectionInput): Promise<number> {
        const result = await this.repository.saveSubjectSelection(subjectSelectionInput);
        return result.id;
    }

    public updateSubjectSelection(subjectSelectionInput: IPutSubjectSelectionInput): Promise<void> {
        return this.repository.updateSubjectSelection(subjectSelectionInput);
    }

    public async deleteSubjectSelection(id: number): Promise<void> {
        await this.getSubjectSelection(id);
        await this.repository.deleteSubjectSelection(id);
    }
}

export default SubjectSelectionManager;
