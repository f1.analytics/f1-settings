import { errorCodes } from '../../../errors';
import { mockDashboardType1, mockDashboardType2, mockSubjectSelectionConfig } from '../integration.test.data';

export const mockValidPostSubjectSelectionInput = {
    config: mockSubjectSelectionConfig,
    dashboardType: mockDashboardType1,
    name: 'Get Driver by Season',
};
export const mockInvalidPostSubjectSelectionInput = {
    dashboardType: mockDashboardType1,
};
export const mockValidPutSubjectSelectionInput = {
    config: mockSubjectSelectionConfig,
    dashboardType: mockDashboardType1,
    id: mockDashboardType1.subjectSelections[0].id,
    name: 'Get Driver by Season',
};
export const mockValidPutSubjectSelectionInput2 = {
    ...mockValidPostSubjectSelectionInput,
    id: 44,
};
export const mockInvalidPutSubjectSelectionInput = {
    id: 54,
};

export const expectedInvalidPostSubjectSelectionInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'config\'',
    },
};
export const expectedInvalidPutSubjectSelectionInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'dashboardType\'',
    },
};
export const expectedSubjectSelectionNotFoundResponse = {
    error: {
        code: errorCodes.SUBJECT_SELECTION_NOT_FOUND,
        message: 'Subject selection 12321 not found',
    },
};
