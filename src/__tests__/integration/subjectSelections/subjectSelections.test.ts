import { Express } from 'express';
import jwt from 'jsonwebtoken';
import request from 'supertest';

import { SubjectSelectionNotFoundError } from '../../../errors/requestErrors';
import SubjectSelectionManager from '../../../managers/subjectSelectionManager';
import { ISubjectSelectionManager } from '../../../managers/subjectSelectionManager/subjectSelectionManager.type';
import { mockSubjectSelection1 } from '../integration.test.data';
import { initServerApp, prepareMockDashboardTypes } from '../integration.test.utils';
import {
    expectedInvalidPostSubjectSelectionInputResponse,
    expectedInvalidPutSubjectSelectionInputResponse,
    expectedSubjectSelectionNotFoundResponse,
    mockInvalidPostSubjectSelectionInput,
    mockInvalidPutSubjectSelectionInput,
    mockValidPostSubjectSelectionInput,
    mockValidPutSubjectSelectionInput,
    mockValidPutSubjectSelectionInput2,
} from './subjectSelections.test.data';

describe('Subject selections integration tests', () => {
    let app: Express;
    let subjectSelectionManager: ISubjectSelectionManager;

    beforeAll(async () => {
        app = await initServerApp();
        subjectSelectionManager = new SubjectSelectionManager();

        await prepareMockDashboardTypes();

        jest.spyOn(jwt, 'verify').mockImplementation((id) => ({ payload: { id: Number(id), isSuperuser: true } }));
    });

    describe('GET / tests', () => {
        it('Should get all subject selections', () => {
            return request(app)
                .get('/subjectSelections/')
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body.length).toBe(3);
                });
        });
    });

    describe('GET /:id tests', () => {
        it('Should get subject selection', () => {
            return request(app)
                .get(`/subjectSelections/${mockSubjectSelection1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toMatchObject(mockSubjectSelection1);
                });
        });
        it('Should get subject selection', () => {
            return request(app)
                .get(`/subjectSelections/${mockSubjectSelection1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toMatchObject(mockSubjectSelection1);
                });
        });
    });
    describe('POST / tests', () => {
        it('Should not create subject selection with invalid input', () => {
            return request(app)
                .post('/subjectSelections')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidPostSubjectSelectionInput)
                .expect(400)
                .expect(expectedInvalidPostSubjectSelectionInputResponse);
        });
        it('Should create subject selection', () => {
            return request(app)
                .post('/subjectSelections')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPostSubjectSelectionInput)
                .expect(200)
                .expect(async (res) => {
                    const subjectSelection = await subjectSelectionManager.getSubjectSelection(res.body.id);
                    await expect(subjectSelection).toMatchObject({
                        config: mockValidPutSubjectSelectionInput.config,
                        name: mockValidPutSubjectSelectionInput.name,
                    });
                });
        });
    });
    describe('PUT / tests', () => {
        it('Should update subject selection', () => {
            return request(app)
                .put('/subjectSelections')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPutSubjectSelectionInput)
                .expect(200)
                .expect(async () => {
                    const subjectSelection =
                        await subjectSelectionManager.getSubjectSelection(mockValidPutSubjectSelectionInput.id);
                    expect(subjectSelection).toMatchObject({
                        config: mockValidPutSubjectSelectionInput.config,
                        name: mockValidPutSubjectSelectionInput.name,
                    });
                });
        });
        it('Should create subject selection', () => {
            return request(app)
                .put('/subjectSelections')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPutSubjectSelectionInput2)
                .expect(200)
                .expect(async () => {
                    const subjectSelection =
                        await subjectSelectionManager.getSubjectSelection(mockValidPutSubjectSelectionInput2.id);
                    expect(subjectSelection).toMatchObject({
                        config: mockValidPutSubjectSelectionInput2.config,
                        name: mockValidPutSubjectSelectionInput2.name,
                    });
                });
        });
        it('Should not create subject selection with invalid input', () => {
            return request(app)
                .put('/subjectSelections')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidPutSubjectSelectionInput)
                .expect(400)
                .expect(expectedInvalidPutSubjectSelectionInputResponse);
        });
    });
    describe('DELETE /:id tests', () => {
        it( 'Should fail to delete non existing subject selection', () => {
            return request(app)
                .delete('/subjectSelections/12321')
                .set('Authorization', 'Bearer 1')
                .expect(404)
                .expect(expectedSubjectSelectionNotFoundResponse);
        });
        it('Should delete subject selection', () => {
            return request(app)
                .delete(`/subjectSelections/${mockSubjectSelection1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect(async () => {
                    expect(subjectSelectionManager.getSubjectSelection(mockSubjectSelection1.id))
                        .toThrowError(SubjectSelectionNotFoundError);
                });
        });
    });
});
