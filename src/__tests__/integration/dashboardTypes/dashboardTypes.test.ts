import { Express } from 'express';
import jwt from 'jsonwebtoken';
import request from 'supertest';

import { DashboardTypeNotFoundError } from '../../../errors/requestErrors';
import DashboardTypeManager, { IDashboardTypeManager } from '../../../managers/dashboardTypeManager';
import { mockDashboardType1, mockDashboardType2, mockDashboardType3 } from '../integration.test.data';
import { initServerApp, prepareMockDashboardTypes } from '../integration.test.utils';
import {
    expectedInvalidPutDashboardTypeInputResponse,
    expectedInvalidSaveDashboardTypeInputResponse,
    expectedNotFoundDashboardTypeResponse,
    mockInvalidPutDashboardTypeInput,
    mockInvalidSaveDashboardTypeInput,
    mockInvalidUpdateDashboardTypeInput,
    mockValidPutDashboardTypeInput,
    mockValidSaveDashboardTypeInput,
    mockValidUpdateDashboardTypeInput,
} from './dashboardTypes.test.data';

describe('Dashboard types integration tests', () => {
    let app: Express;
    let dashboardTypeManager: IDashboardTypeManager;

    beforeAll(async () => {
        app = await initServerApp();
        dashboardTypeManager = new DashboardTypeManager();

        await prepareMockDashboardTypes();

        jest.spyOn(jwt, 'verify').mockImplementation((id) => ({ payload: { id: Number(id), isSuperuser: true } }));
    });

    describe('GET / tests', () => {
        it('Should get all dashboard types', async () => {
            return request(app)
                .get('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(mockDashboardType1).toMatchObject(res.body[0]);
                    expect(mockDashboardType2).toMatchObject(res.body[1]);
                });
        });
    });

    describe('GET /:id tests', () => {
        it('Should get dashboard type', () => {
            return request(app)
                .get(`/dashboardTypes/${mockDashboardType1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toMatchObject(mockDashboardType1);
                });
        });
        it('Should fail to get dashboard type if not found', () => {
            return request(app)
                .get('/dashboardTypes/123123')
                .set('Authorization', 'Bearer 1')
                .expect(404)
                .expect((res) => {
                    expect(res.body).toMatchObject(expectedNotFoundDashboardTypeResponse);
                });
        });
    });

    describe('POST / tests', () => {
        it('Should create a new dashboard type',  () => {
            return request(app)
                .post('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockValidSaveDashboardTypeInput)
                .expect(200)
                .then(async (res) => {
                    const dashboardType = await dashboardTypeManager.getDashboardType(res.body.id);
                    expect(dashboardType).toMatchObject(mockValidSaveDashboardTypeInput);
                });
        });
        it('Should fail to create a new dashboard type with invalid request',  () => {
            return request(app)
                .post('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidSaveDashboardTypeInput)
                .expect(400)
                .expect(expectedInvalidSaveDashboardTypeInputResponse);
        });
    });

    describe('PUT / tests', () => {
        it('Should save a new dashboard type',  () => {
            return request(app)
                .put('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPutDashboardTypeInput)
                .expect(200)
                .then(async () => {
                    const dashboardType =
                        await dashboardTypeManager.getDashboardType(mockValidPutDashboardTypeInput.id);
                    expect(dashboardType).toMatchObject(mockValidPutDashboardTypeInput);
                });
        });
        it('Should fail to save a new dashboard type with invalid request',  () => {
            return request(app)
                .put('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidPutDashboardTypeInput)
                .expect(400)
                .expect(expectedInvalidPutDashboardTypeInputResponse);
        });
        it('Should update existing dashboard type',  () => {
            return request(app)
                .put('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockValidUpdateDashboardTypeInput)
                .expect(200)
                .then(async () => {
                    const dashboard = await dashboardTypeManager.getDashboardType(mockValidUpdateDashboardTypeInput.id);
                    expect(dashboard).toMatchObject(mockValidUpdateDashboardTypeInput);
                });
        });
        it('Should fail to update existing dashboard type',  () => {
            return request(app)
                .put('/dashboardTypes/')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidUpdateDashboardTypeInput)
                .expect(400)
                .expect(expectedInvalidSaveDashboardTypeInputResponse);
        });
    });

    describe('DELETE /:id tests', () => {
        it('Should fail to delete not existing dashboard type', () => {
            return request(app)
                .delete('/dashboardTypes/123123')
                .set('Authorization', 'Bearer 1')
                .expect(404)
                .expect(expectedNotFoundDashboardTypeResponse);
        });
        it('Should delete dashboard type', async () => {
            return request(app)
                .delete(`/dashboardTypes/${mockDashboardType3.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect(async () => {
                    expect(dashboardTypeManager.getDashboardType(mockDashboardType3.id))
                        .toThrowError(DashboardTypeNotFoundError);
                });
        });
    });
});
