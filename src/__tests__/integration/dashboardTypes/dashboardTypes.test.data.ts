import { errorCodes } from '../../../errors';

export const mockValidSaveDashboardTypeInput = {
    icon: 'helmet',
    name: 'testSave1',
};

export const mockInvalidSaveDashboardTypeInput = {
    icon: 'helmet',
    randomField: 'teasdasd',
};
export const mockValidPutDashboardTypeInput = {
    icon: 'helmet',
    id: 69,
    name: 'testPut1',
};
export const mockInvalidPutDashboardTypeInput = {
    icon: 'helmet',
    name: 'invalidPutTest',
};
export const mockValidUpdateDashboardTypeInput = {
    icon: 'helmet',
    id: 69,
    name: 'testPut2',
};
export const mockInvalidUpdateDashboardTypeInput = {
    icon: 'helmet',
    id: 69,
};

export const expectedNotFoundDashboardTypeResponse = {
    error: {
        code: errorCodes.DASHBOARD_TYPE_NOT_FOUND,
        message: 'Dashboard type 123123 not found',
    },
};
export const expectedInvalidSaveDashboardTypeInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'name\'',
    },
};
export const expectedInvalidPutDashboardTypeInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'id\'',
    },
};
