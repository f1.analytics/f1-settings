import { Express } from 'express';
import jwt from 'jsonwebtoken';
import request from 'supertest';

import { DataQueryOptionNotFoundError } from '../../../errors/requestErrors';
import DataQueryOptionManager, { IDataQueryOptionManager } from '../../../managers/dataQueryOptionManager';
import {
    mockDataQueryOption1,
    mockDataQueryOption2,
} from '../integration.test.data';
import { initServerApp, prepareMockDataQueryOptions } from '../integration.test.utils';
import {
    expectedDataQueryOptionNotFoundResponse,
    expectedInvalidPostDataQueryOptionInputResponse,
    expectedInvalidPutDataQueryOptionInputResponse,
    mockInvalidPostDataQueryOptionInput,
    mockInvalidPutDataQueryOptionInput,
    mockValidPostDataQueryOptionInput,
    mockValidPutDataQueryOptionInput,
    mockValidPutDataQueryOptionInput2,
} from './dataQueryOptions.test.data';

describe('Data query options integration tests', () => {
    let app: Express;
    let dataQueryOptionsManager: IDataQueryOptionManager;

    beforeAll(async () => {
        app = await initServerApp();
        dataQueryOptionsManager = new DataQueryOptionManager();

        await prepareMockDataQueryOptions();

        jest.spyOn(jwt, 'verify').mockImplementation((id) => ({ payload: { id: Number(id), isSuperuser: true } }));
    });

    describe('GET / tests', () => {
        it('Should get all data query options', async () => {
            return request(app)
                .get('/dataQueryOptions/')
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(mockDataQueryOption1).toMatchObject(res.body[0]);
                    expect(mockDataQueryOption2).toMatchObject(res.body[1]);
                });
        });
    });
    describe('GET /:id tests', () => {
        it('Should get data query option', () => {
            return request(app)
                .get(`/dataQueryOptions/${mockDataQueryOption1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toMatchObject(mockDataQueryOption1);
                });
        });
        it('Should get data query option', () => {
            return request(app)
                .get(`/dataQueryOptions/${mockDataQueryOption1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect((res) => {
                    expect(res.body).toMatchObject(mockDataQueryOption1);
                });
        });
    });
    describe('POST / tests', () => {
        it('Should not create data query option with invalid input', () => {
            return request(app)
                .post('/dataQueryOptions')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidPostDataQueryOptionInput)
                .expect(400)
                .expect(expectedInvalidPostDataQueryOptionInputResponse);
        });
        it('Should create data query option', () => {
            return request(app)
                .post('/dataQueryOptions')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPostDataQueryOptionInput)
                .expect(200)
                .expect(async (res) => {
                    const dataQueryOption = await dataQueryOptionsManager.getDataQueryOption(res.body.id);
                    await expect(dataQueryOption).toMatchObject(mockValidPostDataQueryOptionInput);
                });
        });
    });
    describe('PUT / tests', () => {
        it('Should update data query option', () => {
            return request(app)
                .put('/dataQueryOptions')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPutDataQueryOptionInput)
                .expect(200)
                .expect(async () => {
                    const dataQueryOption =
                        await dataQueryOptionsManager.getDataQueryOption(mockValidPutDataQueryOptionInput.id);
                    expect(dataQueryOption).toMatchObject(mockValidPutDataQueryOptionInput);
                });
        });
        it('Should create data query option', () => {
            return request(app)
                .put('/dataQueryOptions')
                .set('Authorization', 'Bearer 1')
                .send(mockValidPutDataQueryOptionInput2)
                .expect(200)
                .expect(async () => {
                    const dataQueryOption =
                        await dataQueryOptionsManager.getDataQueryOption(mockValidPutDataQueryOptionInput2.id);
                    expect(dataQueryOption).toMatchObject(mockValidPutDataQueryOptionInput2);
                });
        });
        it('Should not create data query option with invalid input', () => {
            return request(app)
                .put('/dataQueryOptions')
                .set('Authorization', 'Bearer 1')
                .send(mockInvalidPutDataQueryOptionInput)
                .expect(400)
                .expect(expectedInvalidPutDataQueryOptionInputResponse);
        });
    });
    describe('DELETE /:id tests', () => {
        it( 'Should fail to delete non existing data query option', () => {
            return request(app)
                .delete('/dataQueryOptions/12321')
                .set('Authorization', 'Bearer 1')
                .expect(404)
                .expect(expectedDataQueryOptionNotFoundResponse);
        });
        it('Should delete data query option', () => {
            return request(app)
                .delete(`/dataQueryOptions/${mockDataQueryOption1.id}`)
                .set('Authorization', 'Bearer 1')
                .expect(200)
                .expect(async () => {
                    expect(dataQueryOptionsManager.getDataQueryOption(mockDataQueryOption1.id))
                        .toThrowError(DataQueryOptionNotFoundError);
                });
        });
    });
});
