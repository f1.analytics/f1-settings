import { errorCodes } from '../../../errors';
import { DataQueryVariableTypes } from '../../../types';
import { mockDataQueryOption1 } from '../integration.test.data';

export const mockValidPostDataQueryOptionInput = {
    name: 'Get Drivers by Constructor',
    script: 'SELECT * FROM drivers WHERE constructor = {constructor}',
    variables: [{
        availableValues: [] as any,
        exampleValue: 'ferrari',
        name: 'constructor',
        type: DataQueryVariableTypes.String,
    }],
};
export const mockInvalidPostDataQueryOptionInput = {
    name: 'Get Drivers by Constructor',
};
export const mockValidPutDataQueryOptionInput = {
    id: mockDataQueryOption1.id,
    name: 'Get Drivers by Constructor',
    script: 'SELECT * FROM drivers WHERE constructor = {constructor}',
    variables: [{
        availableValues: [] as any,
        exampleValue: 'ferrari',
        name: 'constructor',
        type: DataQueryVariableTypes.String,
    }],
};
export const mockValidPutDataQueryOptionInput2 = {
    ...mockValidPostDataQueryOptionInput,
    id: 44,
};
export const mockInvalidPutDataQueryOptionInput = {
    id: 54,
};

export const expectedInvalidPostDataQueryOptionInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'script\'',
    },
};
export const expectedInvalidPutDataQueryOptionInputResponse = {
    error: {
        code: errorCodes.BAD_REQUEST,
        message: 'should have required property \'name\'',
    },
};
export const expectedDataQueryOptionNotFoundResponse = {
    error: {
        code: errorCodes.DATA_QUERY_OPTION_NOT_FOUND,
        message: 'Data query option 12321 not found',
    },
};
