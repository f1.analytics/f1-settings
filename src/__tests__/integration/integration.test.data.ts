import { DataQueryVariableTypes } from '../../types';

export const mockSubjectSelectionConfig = {
    filterSelect: {
        dataQuery: {
            id: 3,
            variables: [] as any,
        },
        placeholder: 'Select season',
        variables: [{
            exportAs: 'value',
            exportFrom: 'dataQuery',
            exportKey: 'season',
        }],
    },
    selectionTypePlaceholder: 'By Season',
    subjectSelect: {
        dataQuery: {
            id: 4,
            variables: [{
                exportAs: 'season',
                exportFrom: 'filterSelect',
                exportKey: 'season',
            }],
        },
        placeholder: 'Select driver',
        variables: [{
            exportAs: 'value',
            exportFrom: 'dataQuery',
            exportKey: 'id',
        }],
    },
};

export const mockSubjectSelection1 = {
    config: mockSubjectSelectionConfig,
    id: 1,
    name: 'Get Driver by Season',
};
export const mockSubjectSelection2 = {
    config: mockSubjectSelectionConfig,
    id: 2,
    name: 'Get Driver by Season 2',
};
export const mockSubjectSelection3 = {
    config: mockSubjectSelectionConfig,
    id: 3,
    name: 'Get Driver by Season 3',
};

export const mockDashboardType1 = {
    icon: 'helmet',
    id: 200,
    name: 'name1',
    subjectSelections: [
        mockSubjectSelection1,
        mockSubjectSelection2,
    ],
};
export const mockDashboardType2 = {
    icon: 'helmet',
    id: 201,
    name: 'name2',
    subjectSelections: [
        mockSubjectSelection3,
    ],
};
export const mockDashboardType3 = {
    icon: 'helmet',
    id: 202,
    name: 'name3',
};

export const mockDataQueryOption1 = {
    id: 32,
    name: 'Get Drivers',
    script: 'SELECT * FROM drivers',
    variables: [] as any,
};
export const mockDataQueryOption2 = {
    id: 33,
    name: 'Get Drivers by Season',
    script: 'SELECT * FROM drivers WHERE year = {season}',
    variables: [{
        availableValues: [] as any,
        exampleValue: '2019',
        name: 'season',
        type: DataQueryVariableTypes.Number,
    }],
};
export const mockDataQueryOption3 = {
    id: 34,
    name: 'Get Drivers by Nationality',
    script: 'SELECT * FROM drivers WHERE nationality = {nationality}',
    variables: [{
        availableValues: [] as any,
        exampleValue: 'French',
        name: 'nationality',
        type: DataQueryVariableTypes.String,
    }],
};
