import CompositionRoot from '../../compositionRoot';
import DashboardTypeManager from '../../managers/dashboardTypeManager';
import DataQueryOptionManager from '../../managers/dataQueryOptionManager';
import {
    mockDashboardType1,
    mockDashboardType2,
    mockDashboardType3,
    mockDataQueryOption1,
    mockDataQueryOption2,
    mockDataQueryOption3,
} from './integration.test.data';

export const initServerApp = async () => {
    const server = await CompositionRoot.createServer({
        logger: {
            level: 'info',
        },
        persistence: {
            database: ':memory:',
            dropSchema: true,
            logging: false,
            synchronize: true,
            type: 'sqlite',
        },
        server: {
            host: 'test',
            port: 1234,
        },

    });
    const restService = server.getRestService();
    restService.init();
    return restService.getApp();
};

export const prepareMockDashboardTypes = async () => {
    const dashboardTypeManager = new DashboardTypeManager();
    await Promise.all([
        mockDashboardType1,
        mockDashboardType2,
        mockDashboardType3,
    ].map((mockDashboardType) => dashboardTypeManager.saveDashboardType(mockDashboardType)));
};

export const prepareMockDataQueryOptions = async () => {
    const dataQueryOptionManager = new DataQueryOptionManager();
    await Promise.all([
        mockDataQueryOption1,
        mockDataQueryOption2,
        mockDataQueryOption3,
    ].map((mockDataQueryOption) => dataQueryOptionManager.createDataQueryOption(mockDataQueryOption)));
};
