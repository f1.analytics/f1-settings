import { Express } from 'express';
import request from 'supertest';

import { initServerApp, prepareMockDashboardTypes } from '../integration.test.utils';

describe('Settings integration tests', () => {
    let app: Express;

    beforeAll(async () => {
        app = await initServerApp();
        await prepareMockDashboardTypes();
    });

    describe('GET / tests', () => {
        it('Should get general settings', async () => {
            return request(app)
                .get('/')
                .expect(200)
                .expect((res) => {
                    expect(res.body.dashboardTypes).toBeTruthy();
                });
        });
    });
});
