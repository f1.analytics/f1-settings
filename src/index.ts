import 'reflect-metadata';

import config from '../config.json';
import CompositionRoot from './compositionRoot';
import Server from './server';

CompositionRoot.createServer(config).then((server: Server) => {
    server.start();
});
