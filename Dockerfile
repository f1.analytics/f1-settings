FROM node:8
WORKDIR /usr/src/f1-settings
COPY . .
RUN npm install
EXPOSE 80
CMD [ "npm", "start" ]
